
import matplotlib.pyplot as plt
import numpy as np
import os
import PIL
import os
from PIL import Image
import tensorflow as tf



from sklearn.model_selection import train_test_split

from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.models import Sequential
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dense, Flatten, GlobalAveragePooling2D, BatchNormalization, Dropout
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping, ReduceLROnPlateau, CSVLogger
from tensorflow.keras.applications.resnet import ResNet50
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.applications.vgg16 import VGG16
from tensorflow.keras.utils import plot_model
import pathlib

print("OK")
class_names = ['advertisement', 'budget', 'email', 'file folder', 'form', 'handwritten', 'invoice', 'letter']
class_names = ['ADVE', 'Email', 'Form', 'Letter', 'Memo', 'News', 'Note', 'Report', 'Resume', 'Scientific']

def expand2square(pil_img, background_color):
    width, height = pil_img.size
    if width == height:
        return pil_img
    elif width > height:
        result = Image.new(pil_img.mode, (width, width), background_color)
        result.paste(pil_img, (0, (width - height) // 2))
        return result
    else:
        result = Image.new(pil_img.mode, (height, height), background_color)
        result.paste(pil_img, ((height - width) // 2, 0))
        return result

def resize():
    f = r'./dataset/'
    f2 = r'./dataset256/'
    for cl in class_names:
        for file in os.listdir(f + cl):
            if '.jp' in file:
                f_img = f + cl + "/"+file
                img = Image.open(f_img)
                ratio = img.width / img.height
                #img = img.resize((256, 366))
                img.thumbnail((256, 256), Image.ANTIALIAS)
                img = expand2square(img, 'white')
                img.save(f2 + cl + '/' + file)


# 700 / 1000  = 0.7
# 256 , 256 / 0.7

def train():

    data_dir = pathlib.Path("./dataset/")
    image_count = len(list(data_dir.glob('*/*.jpg')))
    print(image_count)

    email = list(data_dir.glob('email/*.jpg'))


    imageWidth = 777
    imageHeight = 1000
    batch_size = 32

    train_ds = tf.keras.preprocessing.image_dataset_from_directory(
        data_dir,
        validation_split=0.2,
        subset="training",
        seed=123,
        image_size=(imageHeight, imageWidth),
        color_mode="grayscale",
        batch_size=batch_size)

    val_ds = tf.keras.preprocessing.image_dataset_from_directory(
        data_dir,
        validation_split=0.2,
        subset="validation",
        seed=123,
        image_size=(imageHeight, imageWidth),
        color_mode="grayscale",
        batch_size=batch_size)


    class_names = train_ds.class_names
    print(class_names)



    # input image dimensions
    img_rows, img_cols = imageWidth, imageHeight
    # The images are RGB.
    img_channels = 3
    class_labels = ['ADVE', 'Email', 'Form', 'Letter', 'Memo', 'News', 'Note', 'Report', 'Resume', 'Scientific']
    class_labels = ['advertisement', 'budget', 'email', 'file folder', 'form', 'handwritten', 'invoice', 'letter']
    nb_classes = len(class_labels)

    datagen = ImageDataGenerator()
    data = datagen.flow_from_directory('./dataset/',
                                        target_size=(imageWidth, imageHeight),
                                        batch_size=100,
                                        class_mode='categorical',
                                        shuffle=True )
    X , y = data.next()
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=11)




    resnet = ResNet50(weights= None, include_top=False, input_shape= (imageWidth,imageHeight,3))
    x = resnet.output
    x = GlobalAveragePooling2D()(x)
    x = Dropout(0.5)(x)
    predictions = Dense(len(class_labels), activation= 'softmax')(x)
    model = Model(inputs = resnet.input, outputs = predictions)
    model.summary()

    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])


    #Logging information
    model_check = ModelCheckpoint('best_model.h5', monitor='val_accuracy', verbose=0, save_best_only=True, mode='max')

    early = EarlyStopping(monitor='val_accuracy', min_delta=0, patience=5, verbose=0, mode='max', restore_best_weights=True)

    reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.2, patience=5, min_lr=0.001)

    csv_logger = CSVLogger('train_log.csv', separator=',')

    #Train
    n_epochs = 50
    history =  model.fit(X_train, y_train,  batch_size = 32, epochs = n_epochs, verbose = 1, 
              validation_data = (X_test, y_test), callbacks = [model_check, early, reduce_lr, csv_logger])



def evaluate_weighted():

    data_dir = pathlib.Path("./dataset/")
    image_count = len(list(data_dir.glob('*/*.jpg')))
    print(image_count)

    email = list(data_dir.glob('email/*.jpg'))


    imageWidth = 777
    imageHeight = 1000
    batch_size = 32

    train_ds = tf.keras.preprocessing.image_dataset_from_directory(
        data_dir,
        validation_split=0.2,
        subset="training",
        seed=123,
        image_size=(imageHeight, imageWidth),
        color_mode="grayscale",
        batch_size=batch_size)

    val_ds = tf.keras.preprocessing.image_dataset_from_directory(
        data_dir,
        validation_split=0.2,
        subset="validation",
        seed=123,
        image_size=(imageHeight, imageWidth),
        color_mode="grayscale",
        batch_size=batch_size)


    class_names = train_ds.class_names
    print(class_names)



    # input image dimensions
    img_rows, img_cols = imageWidth, imageHeight
    # The images are RGB.
    img_channels = 3
    class_labels = ['ADVE', 'Email', 'Form', 'Letter', 'Memo', 'News', 'Note', 'Report', 'Resume', 'Scientific']
    class_labels = ['advertisement', 'budget', 'email', 'file folder', 'form', 'handwritten', 'invoice', 'letter']
    nb_classes = len(class_labels)

    datagen = ImageDataGenerator()
    data = datagen.flow_from_directory('./dataset/',
                                        target_size=(imageWidth, imageHeight),
                                        batch_size=100,
                                        class_mode='categorical',
                                        shuffle=True )
    X , y = data.next()
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=11)




    resnet = ResNet50(weights='imagenet', include_top=False, input_shape= (imageWidth,imageHeight,3))
    x = resnet.output
    x = GlobalAveragePooling2D()(x)
    x = Dropout(0.5)(x)
    predictions = Dense(len(class_labels), activation= 'softmax')(x)
    model = Model(inputs = resnet.input, outputs = predictions)
    model.summary()

    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])


    #Logging information
    model_check = ModelCheckpoint('best_model.h5', monitor='val_accuracy', verbose=0, save_best_only=True, mode='max')

    early = EarlyStopping(monitor='val_accuracy', min_delta=0, patience=5, verbose=0, mode='max', restore_best_weights=True)

    reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.2, patience=5, min_lr=0.001)

    csv_logger = CSVLogger('train_log.csv', separator=',')



resize()

#model = VGG16()
#plot_model(model, to_file='vgg.png')