import matplotlib.pyplot as plt
import numpy as np
import os
import PIL
import tensorflow as tf

from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.models import Sequential

import pathlib

print("OK")

def trainAndSaveModel():
  data_dir = pathlib.Path("./dataset/")
  image_count = len(list(data_dir.glob('*/*.tiff')))
  print(image_count)

  email = list(data_dir.glob('email/*.tiff'))


  imageWidth = 777
  imageHeight = 1000
  batch_size = 32

  train_ds = tf.keras.preprocessing.image_dataset_from_directory(
      data_dir,
      validation_split=0.2,
      subset="training",
      seed=123,
      image_size=(imageHeight, imageWidth),
      color_mode="grayscale",
      batch_size=batch_size)

  val_ds = tf.keras.preprocessing.image_dataset_from_directory(
      data_dir,
      validation_split=0.2,
      subset="validation",
      seed=123,
      image_size=(imageHeight, imageWidth),
      color_mode="grayscale",
      batch_size=batch_size)
    

  class_names = train_ds.class_names
  print(class_names)

  model = Sequential([
    # PREPROCESS IMAGES
    layers.experimental.preprocessing.Resizing(512, 512, input_shape=(imageHeight, imageWidth, 1)),
    layers.experimental.preprocessing.RandomZoom(0.2),
    layers.experimental.preprocessing.Rescaling(1./255),
    
    # FEATURE EXTRACTION
    layers.Conv2D(16, 3, padding='same', activation='relu' ),
    layers.MaxPooling2D(),
    layers.Conv2D(32, 3, padding='same', activation='relu'),
    layers.MaxPooling2D(),
    layers.Conv2D(64, 3, padding='same', activation='relu'),
    layers.MaxPooling2D(),
    #layers.Conv2D(128, 3, padding='same', activation='relu'),
    #layers.MaxPooling2D(),
    #layers.Conv2D(256, 3, padding='same', activation='relu'),
    #layers.MaxPooling2D(),
    #layers.Conv2D(512, 3, padding='same', activation='relu'),
    #layers.MaxPooling2D(),

    # SCALEDOWN TO OUT
    layers.Dropout(0.2),
    layers.Flatten(),
    #layers.Dense(4096, activation='relu'),
    layers.Dense(128, activation='relu'),
    layers.Dense(len(class_names))
  ])

  model.compile(optimizer='adam',
                loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                metrics=['accuracy'])

  model.summary()


  epochs=10
  history = model.fit(
    train_ds,
    validation_data=val_ds,
    epochs=epochs
  )
  model.save("./trainedModel/model")


def loadAndEvaluateModel(modelPath):
  print("Loading", modelPath)
  # It can be used to reconstruct the model identically.
  model = keras.models.load_model(modelPath)
  class_names = ['advertisement', 'budget', 'email', 'file folder', 'form', 'handwritten', 'invoice', 'letter']


  img = keras.preprocessing.image.load_img("./dataset/email/email23.jpg")
  img_array = keras.preprocessing.image.img_to_array(img)
  img_array = tf.expand_dims(img_array, 0) # Create a batch

  predictions = model.predict(img_array)
  score = tf.nn.softmax(predictions[0])

  print(
      "This image most likely belongs to {} with a {:.2f} percent confidence."
      .format(class_names[np.argmax(score)], 100 * np.max(score))
  )


trainAndSaveModel()
#loadAndEvaluateModel("./trainedModel/model")